package com.tidoo.playground

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mynameismidori.currencypicker.CurrencyPicker
import com.tidoo.playground.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val picker = CurrencyPicker.newInstance("Select Currency") // dialog title

        picker.setListener { name, code, symbol, flagDrawableResID ->
            // Implement your code here
            Toast.makeText(applicationContext!!,"name:$name code:$code symbol:$symbol",Toast.LENGTH_LONG).show()
        }
        picker.show(supportFragmentManager, "CURRENCY_PICKER")


    }
}